﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MCTCreateTempLib;
using MCTAnalysisLib;
using Path = System.IO.Path;
using WinForms = System.Windows.Forms; //FolderDialog

namespace MCT_Analyse
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        int questioncounter = 0;
        public bool[] answerkey { get; set; } = new bool[12];

        string sPath;
        string expPath;
        //string filepath;
        bool[] read_answerkey = new bool[12];
        SavingClass SavingTemp = new SavingClass();
        string testname;
        Analyse test = new Analyse();

        //Rectangle[] AnsRects = new Rectangle[12];
        List<Rectangle> AnsRects = new List<Rectangle>();

        //string[] MultipleTests = new string[3];
        List<string> MultipleTests = new List<string>();
        List<string> SavedTests = new List<string>();

        public MainWindow()
        {
            InitializeComponent();
            AnsRects.Add(Ans1);
            AnsRects.Add(Ans2);
            AnsRects.Add(Ans3);
            AnsRects.Add(Ans4);
            AnsRects.Add(Ans5);
            AnsRects.Add(Ans6);
            AnsRects.Add(Ans7);
            AnsRects.Add(Ans8);
            AnsRects.Add(Ans9);
            AnsRects.Add(Ans10);
            AnsRects.Add(Ans11);
            AnsRects.Add(Ans12);

        }
       



        private void btSave_Click(object sender, RoutedEventArgs e)
        {
            List<Rectangle> RectsFilled = new List<Rectangle>();
            try
            {
                RectsFilled = AnsRects;
                foreach (Rectangle rect in AnsRects)
                {
                    rect.Fill = Brushes.Transparent;
                }
                Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
                dlg.FileName = SavingTemp.Title; // Default file name
                dlg.DefaultExt = ".png"; // Default file extension
                dlg.Filter = "PNG File (.png)|*.png"; // Filter files by extension

                // Show save file dialog box
                Nullable<bool> result = dlg.ShowDialog();

                // Process save file dialog box results
                if (result == true)
                {
                    // Canvas speichern




                    string filename = dlg.FileName;
                    SavingTemp.Filename = filename;
                    SavingTemp.Canvas = MCTCanvas;
                    SavingTemp.SaveCanvasAsImageDialog(this);
                    SavingTemp.SaveCanvasAsImage();

                }
                int i = 0;
                foreach (Rectangle rectangle in AnsRects)
                {
                    if (answerkey[i])
                        rectangle.Fill = Brushes.Black;
                    i++;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void btNextQuestion_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                if (questioncounter == 0)
                {
                    tblockQ1.Text = tbQuestion.Text;
                    tblockA1.Text = tbAnswerA.Text;
                    tblockB1.Text = tbAnswerB.Text;
                    tblockC1.Text = tbAnswerC.Text;
                    if (string.IsNullOrWhiteSpace(tblockQ1.Text) || string.IsNullOrWhiteSpace(tblockA1.Text) || string.IsNullOrWhiteSpace(tblockB1.Text) || string.IsNullOrWhiteSpace(tblockC1.Text))
                        throw new Exception("Fragen oder Antworten dürfen nicht leer sein!");
                    lbQCounter.Content = "Frage 2/4";
                    if (cbA.IsChecked == true)
                    {
                        AnsRects[0].Fill = Brushes.Black;
                        answerkey[0] = true;
                    }
                    if (cbB.IsChecked == true)
                    {
                        AnsRects[1].Fill = Brushes.Black;
                        answerkey[1] = true;
                    }
                    if (cbC.IsChecked == true)
                    {
                        AnsRects[2].Fill = Brushes.Black;
                        answerkey[2] = true;
                    }
                }
                if (questioncounter == 1)
                {
                    tblockQ2.Text = tbQuestion.Text;
                    tblockA2.Text = tbAnswerA.Text;
                    tblockB2.Text = tbAnswerB.Text;
                    tblockC2.Text = tbAnswerC.Text;
                    if (string.IsNullOrWhiteSpace(tblockQ2.Text) || string.IsNullOrWhiteSpace(tblockA2.Text) || string.IsNullOrWhiteSpace(tblockB2.Text) || string.IsNullOrWhiteSpace(tblockC2.Text))
                        throw new Exception("Fragen oder Antworten dürfen nicht leer sein!");
                    lbQCounter.Content = "Frage 3/4";
                    if (cbA.IsChecked == true)
                    {
                        AnsRects[3].Fill = Brushes.Black;
                        answerkey[3] = true;
                    }
                    if (cbB.IsChecked == true)
                    {
                        AnsRects[4].Fill = Brushes.Black;
                        answerkey[4] = true;
                    }
                    if (cbC.IsChecked == true)
                    {
                        AnsRects[5].Fill = Brushes.Black;
                        answerkey[5] = true;
                    }
                }
                if (questioncounter == 2)
                {
                    tblockQ3.Text = tbQuestion.Text;
                    tblockA3.Text = tbAnswerA.Text;
                    tblockB3.Text = tbAnswerB.Text;
                    tblockC3.Text = tbAnswerC.Text;
                    if (string.IsNullOrWhiteSpace(tblockQ3.Text) || string.IsNullOrWhiteSpace(tblockA3.Text) || string.IsNullOrWhiteSpace(tblockB3.Text) || string.IsNullOrWhiteSpace(tblockC3.Text))
                        throw new Exception("Fragen oder Antworten dürfen nicht leer sein!");
                    lbQCounter.Content = "Frage 4/4";
                    if (cbA.IsChecked == true)
                    {
                        AnsRects[6].Fill = Brushes.Black;
                        answerkey[6] = true;
                    }
                    if (cbB.IsChecked == true)
                    {
                        AnsRects[7].Fill = Brushes.Black;
                        answerkey[7] = true;
                    }
                    if (cbC.IsChecked == true)
                    {
                        AnsRects[8].Fill = Brushes.Black;
                        answerkey[8] = true;
                    }
                }
                if (questioncounter == 3)
                {
                    tblockQ4.Text = tbQuestion.Text;
                    tblockA4.Text = tbAnswerA.Text;
                    tblockB4.Text = tbAnswerB.Text;
                    tblockC4.Text = tbAnswerC.Text;
                    if (string.IsNullOrWhiteSpace(tblockQ4.Text) || string.IsNullOrWhiteSpace(tblockA4.Text) || string.IsNullOrWhiteSpace(tblockB4.Text) || string.IsNullOrWhiteSpace(tblockC4.Text))
                        throw new Exception("Fragen oder Antworten dürfen nicht leer sein!");
                    lbQCounter.Content = "Frage --";
                    if (cbA.IsChecked == true)
                    {
                        AnsRects[9].Fill = Brushes.Black;
                        answerkey[9] = true;
                    }
                    if (cbB.IsChecked == true)
                    {
                        AnsRects[10].Fill = Brushes.Black;
                        answerkey[10] = true;
                    }
                    if (cbC.IsChecked == true)
                    {
                        AnsRects[11].Fill = Brushes.Black;
                        answerkey[11] = true;
                    }
                    btNextQuestion.Content = "Titel ändern";
                }
                lbCanvasTitel.Content = tbTitel.Text;
                testname = tbTitel.Text;
                SavingTemp.Title = tbTitel.Text;
                questioncounter++;

                //Zurücksetzen des Formulars
                tbQuestion.Text = "";
                tbAnswerA.Text = "";
                tbAnswerB.Text = "";
                tbAnswerC.Text = "";
                cbA.IsChecked = false;
                cbB.IsChecked = false;
                cbC.IsChecked = false;

                SavingTemp.Answerkey = answerkey;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void btSaveCheatsheet_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                foreach (Rectangle rect in AnsRects)
                {
                    rect.Fill = Brushes.Transparent;
                }
                WinForms.FolderBrowserDialog folderDialog = new WinForms.FolderBrowserDialog();
                folderDialog.ShowNewFolderButton = true;
                folderDialog.SelectedPath = System.AppDomain.CurrentDomain.BaseDirectory;
                WinForms.DialogResult result = folderDialog.ShowDialog();

                if (result == WinForms.DialogResult.OK)
                {
                    //----< Selected Folder >----
                    //< Selected Path >
                    sPath = folderDialog.SelectedPath;


                    SavingTemp.SaveAnswerkey(sPath);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btOpenKey_Click(object sender, RoutedEventArgs e)
        {
            //OpenFileDialog choosefile = new OpenFileDialog();
            //choosefile.Filter = "Text Documents (*.txt)|*.txt|All Files (*.*)|*.*";
            //choosefile.FilterIndex = 1;
            //choosefile.Multiselect = false;

            //if (choosefile.ShowDialog() == true)
            //{
            //    filepath = choosefile.FileName;
            //}   
            //else
            //{
            //    filepath = string.Empty;
            //}

            //string line;
            //StreamReader sr = new StreamReader(filepath);
            //List<bool> list = new List<bool>();
            //while (!sr.EndOfStream)
            //{
            //    line = sr.ReadLine();
            //    list.Add(bool.Parse(line));
            //}

            //read_answerkey = list.ToArray();
        }

        private void BtnLoadImage_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = "Image files (*.png;*.jpeg;*.jpg)|*.png;*.jpeg;*.jpg|All files (*.*)|*.*)";
                if (openFileDialog.ShowDialog() == true)
                    test.InputImagePath = openFileDialog.FileName;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnSaveAnalyzedTest_Click(object sender, RoutedEventArgs e)
        {


            try
            {
                if (txtStudentName.Text != "Schüler Name (optional)" && txtStudentName.Text != "" && txtStudentName.Text != " ")
                {
                    if (txtTestName.Text != "Test Name (optional)" && txtTestName.Text != "" && txtTestName.Text != " ")
                        test.ExportToCsv(txtStudentName.Text, txtTestName.Text);
                    else
                        test.ExportToCsv(txtStudentName.Text);
                }
                else if (txtTestName.Text != "Test Name (optional)" && txtTestName.Text != "" && txtTestName.Text != " ")
                    test.ExportToCsv(txtTestName.Text, true);
                else
                    test.ExportToCsv();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BtnMutliple_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = "Image files (*.png;*.jpeg;*.jpg)|*.png;*.jpeg;*.jpg|All files (*.*)|*.*)";
                openFileDialog.Multiselect = true;
                if (openFileDialog.ShowDialog() == true)
                {
                    foreach (string filename in openFileDialog.FileNames)
                    {
                        //MultipleTests[fillarray] = filename;
                        MultipleTests.Add(filename);
                        SavedTests.Add(filename);
                    }
                    if (MultipleTests.Count == 1)
                        txtStudentName.Visibility = Visibility.Visible;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BtnChooseAnswerkey_Click(object sender, RoutedEventArgs e)
        {
            ChooseAnswerkey dlg = new ChooseAnswerkey();

            try
            {

                if (dlg.ShowDialog().Value)
                {


                }

            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }
            test.SetAnswerkey(dlg.SelAnswerkey);
            test.NameOfAnswerkey = dlg.TestName;
        }



        private void btnSaveMultiple_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                WinForms.FolderBrowserDialog folderDialog = new WinForms.FolderBrowserDialog();
                folderDialog.ShowNewFolderButton = true;
                folderDialog.SelectedPath = System.AppDomain.CurrentDomain.BaseDirectory;
                WinForms.DialogResult result = folderDialog.ShowDialog();

                if (result == WinForms.DialogResult.OK)
                {
                    //----< Selected Folder >----
                    //< Selected Path >
                    expPath = folderDialog.SelectedPath;
                    test.ExportPath = expPath;
                }
                if (SavedTests.Count > 1)
                {
                    if (txtTestName.Text == "Test Name (optional)" || string.IsNullOrWhiteSpace(txtTestName.Text))
                    {
                        foreach (string file in SavedTests)
                        {
                            test.ExportMultiple(file);
                        }
                    }
                    else
                    {
                        foreach (string file in SavedTests)
                        {
                            test.ExportMultiple(file, txtTestName.Text);
                        }
                    }

                }
                else if (SavedTests.Count == 1)
                {
                    if (txtStudentName.Text != "Schüler Name (optional)" && txtStudentName.Text != "" && txtStudentName.Text != " ")
                    {
                        if (txtTestName.Text != "Test Name (optional)" && txtTestName.Text != "" && txtTestName.Text != " ")
                            test.ExportToCsv(txtStudentName.Text, txtTestName.Text);
                        else
                            test.ExportToCsv(txtStudentName.Text);
                    }
                    else if (txtTestName.Text != "Test Name (optional)" && txtTestName.Text != "" && txtTestName.Text != " ")
                        test.ExportToCsv(txtTestName.Text, true);
                    else
                        test.ExportToCsv();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void btnAnalyseMultiple_Click_1(object sender, RoutedEventArgs e)
        {

            //test.SetAnswerkey(answerkey);
            try
            {
                if (MultipleTests.Count > 1)
                {
                    ImgOutput.Visibility = Visibility.Hidden;
                    foreach (string file in MultipleTests)
                    {
                        test.InputImagePath = file;
                        ListOutput.Items.Add(test.AnalyseMultiple());

                    }
                    MultipleTests.Clear();
                }
                else if (MultipleTests.Count == 1)
                {
                    ImgOutput.Visibility = Visibility.Visible;
                    test.InputImagePath = MultipleTests[0];
                    ImgOutput.Source = MCTAnalysisLib.BitmapSourceExtension.ToBitmapSource(test.AnalyseTheTest());
                    MultipleTests.Clear();
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }



        }

        private void txtTestName_GotFocus(object sender, RoutedEventArgs e)
        {
            txtTestName.Text = "";
        }

        private void txtStudentName_GotFocus(object sender, RoutedEventArgs e)
        {
            txtStudentName.Text = "";
        }
    }
}
