﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MCTCreateTempLib
{
    public class SavingClass
    {
        private bool[] answerkey=new bool[12];

        public bool[] Answerkey
        {
            get { return answerkey; }
            set { answerkey = value; }
        }

        private string title="";

        public string Title
        {
            get { return title; }
            set { title = value; }
        }

        private Canvas canvas;

        public Canvas Canvas
        {
            get { return canvas; }
            set { canvas = value; }
        }

        private string filename;

        public string Filename
        {
            get { return filename; }
            set { filename = value; }
        }


        public SavingClass()
        {

        }

        public SavingClass(Canvas canvas, string filename)
        {
            Canvas = canvas;
            Filename = filename;
        }
        public void SaveCanvasAsImage()
        {
            if (string.IsNullOrWhiteSpace(Title))
                throw new Exception("Titel darf nicht leer sein!");
            if(string.IsNullOrWhiteSpace(Filename))
                throw new Exception("Filename darf nicht leer sein!");
            RenderTargetBitmap renderBitmap = new RenderTargetBitmap((int)canvas.Width, (int)canvas.Height, 96d, 96d, PixelFormats.Pbgra32);
            canvas.Measure(new Size((int)canvas.Width, (int)canvas.Height));
            canvas.Arrange(new Rect(new Size((int)canvas.Width, (int)canvas.Height)));
            renderBitmap.Render(canvas);
            //JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            PngBitmapEncoder encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(renderBitmap));

            using (FileStream file = File.Create(filename))
            {
                encoder.Save(file);
            }
        }

        public void SaveCanvasAsImageDialog(Window window)
        {
            if (string.IsNullOrWhiteSpace(Title))
                throw new Exception("Titel darf nicht leer sein!");
            Size size = new Size(window.Width, window.Height);
            canvas.Measure(size);
            //canvas.Arrange(new Rect(size));
            RenderTargetBitmap rtb = new RenderTargetBitmap(
                (int)window.Width, //width
                (int)window.Height, //height
                96, //dpi x
                96, //dpi y
                PixelFormats.Pbgra32 // pixelformat
                );
            rtb.Render(canvas);
        }

        public void PrintCanvas(Canvas printcanvas)
        {
            if (string.IsNullOrWhiteSpace(Title))
                throw new Exception("Titel darf nicht leer sein!");
            PrintDialog printbr = new PrintDialog();
            if (printbr.ShowDialog() == true)
            {
                printbr.PrintVisual(printcanvas, "Printing Canvas");
            }
        }

        public void SaveAnswerkey(string path)
        {
            if (string.IsNullOrWhiteSpace(Title))
                throw new Exception("Titel darf nicht leer sein!");
            bool exists = true;
            
            string root = path;
            string filename = "Answerkeys.csv";
            if (!Directory.Exists(root))
            {
                Directory.CreateDirectory(root);
            }
           
            
            filename = $"{root}\\{filename}";
            string name = title;
            string analysed = $"{name};{Answerkey[0]};{Answerkey[1]};{Answerkey[2]};{Answerkey[3]};{Answerkey[4]};{Answerkey[5]};{Answerkey[6]};{Answerkey[7]};{Answerkey[8]};{Answerkey[9]};{Answerkey[10]};{Answerkey[11]};";
            if (!File.Exists(filename))
            {
                exists = false;
            }
            using (FileStream fs = new FileStream(filename, FileMode.Append, FileAccess.Write))
            {
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    if (!exists)
                        sw.WriteLine("Name;Answer 1;Answer 2;Answer 3;Answer 4;Answer 5;Answer 6;Answer 7;Answer 8;Answer 9;Answer 10;Answer 11;Answer 12");
                    sw.WriteLine(analysed);
                    sw.Dispose();
                }
            }

            //SaveFileDialog saveFileDialogtxt = new SaveFileDialog();
            //saveFileDialogtxt.DefaultExt = ".txt";
            //saveFileDialogtxt.Filter = "Text Documents (*.txt)|*.txt|All Files (*.*)|*.*";
            //if (saveFileDialogtxt.ShowDialog() == true)
            //{
            //    try
            //    {
            //        using (Stream s = File.Open(saveFileDialogtxt.FileName, FileMode.CreateNew))
            //        using (StreamWriter sw = new StreamWriter(s))
            //        {
            //            for (int i = 0; i < answerkey.Length; i++)
            //            {
            //                sw.WriteLine(answerkey[i]);
            //            }
            //        }
            //    }
            //    catch (Exception)
            //    {
            //        MessageBox.Show("Unbekannter Fehler: Bitte wählen Sie einen anderen Namen oder löschen sie die gleichnamige Datei.");
            //    }
            //}
        }


    }
}







