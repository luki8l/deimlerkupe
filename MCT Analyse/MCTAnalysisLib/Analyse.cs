﻿using System;
using System.Collections.Generic;
using System.Linq;
using Emgu.CV;
using Emgu.CV.Structure;
using System.IO;
using Emgu.CV.Util;

using Path = System.IO.Path;
using System.Drawing;

namespace MCTAnalysisLib
{
    public class Analyse
    {
        #region Globale Variablen
        //public string filename;
        //public string filename;
        Image<Gray, byte> imgCanny;
        Image<Gray, byte> imgPaper;
        Image<Gray, byte> imgBinarize;
        Image<Bgr, byte> imgOld;
        Image<Gray, byte> imgGray;

        Image<Bgr, byte> imgout;


        VectorOfPoint docCnt;

        //bool[] answerkey = { true, true, true, false, true, false, true, false, true, false, true, true };

        int group1, group2, group3, group4;
        double overall;
        int Note;
        bool[] answered = new bool[12];
        private string inputImagePath;
        private bool[] Correctanswers = new bool[12];

        Dictionary<int, int> bubbles = new Dictionary<int, int>();
        MCvScalar green = new MCvScalar(0, 255, 0);
        MCvScalar red = new MCvScalar(0, 0, 255);

        int answerkeyindex = 0;

        bool filled = false;

        int index = 0;
        List<int> Points = new List<int>();
        List<int> Noten = new List<int>();
        List<double> Prozent = new List<double>();
        #endregion
        public Analyse(string nameofimage, bool[] correctanswers)
        {
            Correctanswers = correctanswers;
            if (Correctanswers.Length < 12 || Correctanswers.Length > 12)
                throw new Exception("Der Answerkey hat das falsche Format");
            InputImagePath = nameofimage;
            if (string.IsNullOrWhiteSpace(inputImagePath))
                throw new Exception("Es wurde kein Bild angegeben");
        }
        public Analyse()
        {

        }
        #region Properties

        private string exportPath;

        public string ExportPath
        {
            get { return exportPath; }
            set { exportPath = value; }
        }


        private string nameOfAnswerkey;

        public string NameOfAnswerkey
        {
            get { return nameOfAnswerkey; }
            set { nameOfAnswerkey = value; }
        }

        public void SetAnswerkey(bool[] answers)
        {
            if (answers.Length < 12 || answers.Length > 12)
                throw new Exception("Der Answerkey hat das falsche Format");
            Correctanswers = answers;
        }
        public bool[] GetAnswerkey()
        {
            return answered;
        }
        public string InputImagePath
        {
            get { return inputImagePath; }
            set 
            { 
                inputImagePath = value;
                if (string.IsNullOrWhiteSpace(inputImagePath))
                    throw new Exception("Es wurde kein Bild angegeben");
            }
        }
        #endregion


        public Image<Bgr, byte> AnalyseTheTest()
        {
            
            EdgeDetection();
            ContourDetection();
            Binarize();
            OpticAnalysis();
            Image<Bgr, byte> Calculated = Calculation();
            return Calculated;
        }

        public string AnalyseMultiple()
        {
            EdgeDetection();
            ContourDetection();
            Binarize();
            OpticAnalysisMultiple();
            string output = CalculationMultiple();
            return output;
        }
        #region Funktionen
        private void EdgeDetection()
        {
            imgOld = new Image<Bgr, byte>(inputImagePath); //Liest altes Bild ein
            imgCanny = new Image<Gray, byte>(imgOld.Width, imgOld.Height, new Gray(0)); //Erstellt graues Bild welches die Größe des eingelesen Bildes besitzt
            imgCanny = imgOld.Canny(75, 200); //Canny findet die Ecken und markiert diese
            //ImageGray.Source = EdgeDetectionLib.BitmapSourceExtension.ToBitmapSource(imgCanny);
            //ImageAnswers.Source = EdgeDetectionLib.BitmapSourceExtension.ToBitmapSource(imgCanny);
        }

        private void ContourDetection()
        {

            VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint();
            Mat m = new Mat();

            CvInvoke.FindContours(imgCanny, contours, m, Emgu.CV.CvEnum.RetrType.External, Emgu.CV.CvEnum.ChainApproxMethod.ChainApproxSimple); // Konturen finden, Benötigt imgCanny von btnEdgeDetection

            for (int i = 0; i < contours.Size; i++)
            {
                double perimeter = CvInvoke.ArcLength(contours[i], true);
                VectorOfPoint approx = new VectorOfPoint();
                CvInvoke.ApproxPolyDP(contours[i], approx, 0.02 * perimeter, true);

                //CvInvoke.DrawContours(imgCanny, contours, i, new MCvScalar(0, 0, 255), 2); //Konturen auf das Bild zeichnen


                if (approx.Size == 4) // 4 Ecken -> Das Papier
                {
                    docCnt = approx;
                }

            }

            //ImageContur.Source = EdgeDetectionLib.BitmapSourceExtension.ToBitmapSource(imgCanny);


            imgPaper = imgCanny; // Für Testzwecke
            Rectangle paper = CvInvoke.BoundingRectangle(docCnt);

            //imgPaper.ROI = new Rectangle(paper.X, paper.Y, paper.Width, paper.Height);
            //ImageAnswers.Source = EdgeDetectionLib.BitmapSourceExtension.ToBitmapSource(imgPaper);
            //imgOld.ROI = new Rectangle(paper.X, paper.Y, paper.Width, paper.Height);

            // ImageContur.Source = EdgeDetectionLib.BitmapSourceExtension.ToBitmapSource(imgPaper);
        }

        private void Binarize()
        {
            imgBinarize = new Image<Gray, byte>(imgPaper.Width, imgPaper.Height, new Gray(0));
            imgGray = new Image<Gray, byte>(imgPaper.Width, imgPaper.Height, new Gray(0)); // Bild mit selber Größe erstellt
            CvInvoke.CvtColor(imgOld, imgGray, Emgu.CV.CvEnum.ColorConversion.Bgr2Gray, 0); // Gegrayscaled
            CvInvoke.Threshold(imgGray, imgBinarize, 0, 255, Emgu.CV.CvEnum.ThresholdType.BinaryInv | Emgu.CV.CvEnum.ThresholdType.Otsu); //Binarized auf Schwarz / Weiß
            // ImageAnswers.Source = EdgeDetectionLib.BitmapSourceExtension.ToBitmapSource(imgBinarize);
            // ImageBinarized.Source = EdgeDetectionLib.BitmapSourceExtension.ToBitmapSource(imgBinarize); 
        }

        private void OpticAnalysis()
        {
            answerkeyindex = 0;
            VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint();
            Mat m = new Mat();

            CvInvoke.FindContours(imgBinarize, contours, m, Emgu.CV.CvEnum.RetrType.External, Emgu.CV.CvEnum.ChainApproxMethod.ChainApproxSimple); // Konturen finden, Benötigt imgBinarized

            Dictionary<int, double> dict = new Dictionary<int, double>(); // hier werden die Antworten abgespeichert: <int wievielte Contur, double y höhe>

            if (contours.Size < 0)
                return;

            for (int i = 0; i < contours.Size; i++)
            {
                //double area = CvInvoke.ContourArea(contours[i]);

                System.Drawing.Rectangle rect = CvInvoke.BoundingRectangle(contours[i]);
                //CvInvoke.Rectangle(imgOld, rect, new MCvScalar(255, 0, 0), 3); //Blaue Rechtecke um alle Felder TEST    
                //System.Drawing.Rectangle rect = CvInvoke.BoundingRectangle(contours[key]);
                double yvalue = rect.Y;
                if (rect.Height > 35 && rect.Height < 43 && rect.Width < 43 && rect.Width > 35) // Antwortfelder rausfiltern
                {
                    dict.Add(i, yvalue); // In Dictionary einfügen
                }
            }

            var item = dict.OrderBy(v => v.Value); // Sortiert von oben nach unten
            if (dict.Count < 12 || dict.Count > 12)
                throw new Exception("Bild ist zu schlecht aufgenommen");
            imgout = new Image<Bgr, byte>(imgBinarize.Width, imgBinarize.Height, new Bgr(0, 0, 0));
            imgout = imgOld.Copy();
            foreach (var it in item)
            {

                int key = int.Parse(it.Key.ToString());
                Rectangle rect = CvInvoke.BoundingRectangle(contours[key]); // Rechteck um Antwortfelder
                //CvInvoke.Rectangle(imgout, rect, new MCvScalar(255, 0, 0), 3); //TEST

                /*
                if (test< 3)
                    CvInvoke.Rectangle(imgOld, rect, new MCvScalar(255,0,0), 3); //Blaue Rechtecke um alle Felder TEST     
                if (test> 2 && test< 6)
                    CvInvoke.Rectangle(imgOld, rect, new MCvScalar(0,255,0), 3); //Grüne Rechtecke um alle Felder TEST
                if (test> 5)
                    CvInvoke.Rectangle(imgOld, rect, new MCvScalar(0, 0, 255), 3); //Rote Rechtecke um alle Felder TEST
                test++;
                */

                //imgOld.ROI = new Rectangle(rect.X, rect.Y, rect.Width, rect.Height); // Ansicht auf das Antwortkreuz beschränken
                //Bgr image = imgOld.GetAverage(); // Durchschnittsfarbanteil
                //string value = image.Blue.ToString();

                imgBinarize.ROI = new Rectangle(rect.X, rect.Y, rect.Width, rect.Height); // Ansicht auf das Antwortkreuz beschränken
                Gray image = imgBinarize.GetAverage(); // Durchschnittsfarbanteil
                string value = image.ToString();
                double a = Convert.ToDouble(value.Substring(1, 3));


                if (a > 200)
                {
                    filled = true;
                    answered[answerkeyindex] = true;
                }

                else
                {
                    filled = false;
                    answered[answerkeyindex] = false;
                }


                if (Correctanswers[answerkeyindex] == true && filled == true)
                {
                    CvInvoke.Rectangle(imgout, rect, green, 3);
                }

                else if (Correctanswers[answerkeyindex] == true && filled == false)
                {
                    CvInvoke.Rectangle(imgout, rect, red, 3);
                }
                else if (Correctanswers[answerkeyindex] == false && filled == true)
                {
                    CvInvoke.Rectangle(imgout, rect, red, 3);
                }

                answerkeyindex++;

            }

        }
        private void OpticAnalysisMultiple()
        {
            answerkeyindex = 0;
            VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint();
            Mat m = new Mat();

            CvInvoke.FindContours(imgBinarize, contours, m, Emgu.CV.CvEnum.RetrType.External, Emgu.CV.CvEnum.ChainApproxMethod.ChainApproxSimple); // Konturen finden, Benötigt imgBinarized

            Dictionary<int, double> dict = new Dictionary<int, double>(); // hier werden die Antworten abgespeichert: <int wievielte Contur, double y höhe>

            if (contours.Size < 0)
                return;

            for (int i = 0; i < contours.Size; i++)
            {
                //double area = CvInvoke.ContourArea(contours[i]);

                System.Drawing.Rectangle rect = CvInvoke.BoundingRectangle(contours[i]);
                //CvInvoke.Rectangle(imgOld, rect, new MCvScalar(255, 0, 0), 3); //Blaue Rechtecke um alle Felder TEST    
                //System.Drawing.Rectangle rect = CvInvoke.BoundingRectangle(contours[key]);
                double yvalue = rect.Y;
                if (rect.Height > 35 && rect.Height < 43 && rect.Width < 43 && rect.Width > 35) // Antwortfelder rausfiltern
                {
                    dict.Add(i, yvalue); // In Dictionary einfügen
                }
            }

            var item = dict.OrderBy(v => v.Value); // Sortiert von oben nach unten
            if (dict.Count < 12 || dict.Count>12)
                throw new Exception($"{InputImagePath} ist zu schlecht aufgenommen");
            imgout = new Image<Bgr, byte>(imgBinarize.Width, imgBinarize.Height, new Bgr(0, 0, 0));
            imgout = imgOld.Copy();
            foreach (var it in item)
            {

                int key = int.Parse(it.Key.ToString());
                Rectangle rect = CvInvoke.BoundingRectangle(contours[key]); // Rechteck um Antwortfelder


                /*
                if (test< 3)
                    CvInvoke.Rectangle(imgOld, rect, new MCvScalar(255,0,0), 3); //Blaue Rechtecke um alle Felder TEST     
                if (test> 2 && test< 6)
                    CvInvoke.Rectangle(imgOld, rect, new MCvScalar(0,255,0), 3); //Grüne Rechtecke um alle Felder TEST
                if (test> 5)
                    CvInvoke.Rectangle(imgOld, rect, new MCvScalar(0, 0, 255), 3); //Rote Rechtecke um alle Felder TEST
                test++;
                */

                //imgOld.ROI = new Rectangle(rect.X, rect.Y, rect.Width, rect.Height); // Ansicht auf das Antwortkreuz beschränken
                //Bgr image = imgOld.GetAverage(); // Durchschnittsfarbanteil
                //string value = image.Blue.ToString();

                imgBinarize.ROI = new Rectangle(rect.X, rect.Y, rect.Width, rect.Height); // Ansicht auf das Antwortkreuz beschränken
                Gray image = imgBinarize.GetAverage(); // Durchschnittsfarbanteil
                string value = image.ToString();
                double a = Convert.ToDouble(value.Substring(1, 3));


                if (a > 200)
                {
                    filled = true;
                    answered[answerkeyindex] = true;
                }

                else
                {
                    filled = false;
                    answered[answerkeyindex] = false;
                }
                /*

                if (Correctanswers[answerkeyindex] == true && filled == true)
                {
                    CvInvoke.Rectangle(imgout, rect, green, 3);
                }

                else if (Correctanswers[answerkeyindex] == true && filled == false)
                {
                    CvInvoke.Rectangle(imgout, rect, red, 3);
                }
                else if (Correctanswers[answerkeyindex] == false && filled == true)
                {
                    CvInvoke.Rectangle(imgout, rect, red, 3);
                }
                */
                answerkeyindex++;
            }
        }
        #region exports
        public void ExportToCsv(string name) // Nur Name des Schülers verwenden, Testname aus Syntax
        {
            string root = ExportPath;
            if (!Directory.Exists(root))
            {
                Directory.CreateDirectory(root);
            }
            
            string analysed = $"{name};{group1 + group2 + group3 + group4};{Math.Round(overall, 2)}%;{Note}";
            string filename = NameOfAnswerkey;
            filename = $"{root}\\{filename}";
            bool exists = true;
            if (!File.Exists($"{filename}.csv"))
            {
                exists = false;
            }
            using (FileStream fs = new FileStream($"{filename}.csv", FileMode.Append, FileAccess.Write))
            {
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    if (!exists)
                        sw.WriteLine("Name;Punkte von 4;Prozent;Note");
                    sw.WriteLine(analysed);
                    sw.Dispose();
                }
            }
        }
        public void ExportToCsv(string testname, bool OnlyTestname) // Nur Name des Schülers verwenden, Testname aus Syntax
        {
            if (!OnlyTestname)
                throw new Exception("OnlyTestname Must be true");
            string root = ExportPath;
            if (!Directory.Exists(root))
            {
                Directory.CreateDirectory(root);
            }
            string imagename = Path.GetFileName(inputImagePath);
            string name = $"{imagename.Substring(0, imagename.IndexOf('-'))} {imagename.Substring(imagename.IndexOf('-') + 1, imagename.IndexOf('.')- imagename.IndexOf('-') + 1)}";
            string analysed = $"{name};{group1 + group2 + group3 + group4};{Math.Round(overall, 2)}%;{Note}";
            string filename = $"{testname}.csv";
            filename = $"{root}\\{filename}";
            bool exists = true;
            if (!File.Exists(filename))
            {
                exists = false;
            }
            using (FileStream fs = new FileStream(filename, FileMode.Append, FileAccess.Write))
            {
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    if (!exists)
                        sw.WriteLine("Name;Punkte von 4;Prozent;Note");
                    sw.WriteLine(analysed);
                    sw.Dispose();
                }
            }
        }
        public void ExportToCsv(string name, string testname) // Name des Schülers und Testname manuell
        {
            string analysed = $"{name};{group1 + group2 + group3 + group4};{Math.Round(overall, 2)}%;{Note}";
            bool exists = true;
            string root = ExportPath;
            if (!Directory.Exists(root))
            {
                Directory.CreateDirectory(root);
            }
            if (!File.Exists($"{root}\\{testname}.csv"))
            {
                exists = false;
            }
            using (FileStream fs = new FileStream($"{root}\\{testname}.csv", FileMode.Append, FileAccess.Write))
            {
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    if (!exists)
                        sw.WriteLine("Name;Punkte von 4;Prozent;Note");
                    sw.WriteLine(analysed);
                    sw.Dispose();
                }
            }
        }

        public void ExportMultiple(string nameofimage) // Mehrere
        {
            
            bool exists = true;
            string root = ExportPath;
            if (!Directory.Exists(root))
            {
                Directory.CreateDirectory(root);
            }

            int Punkte = Points[index];
            int Grade = Noten[index];
            double overall = Prozent[index];
            index++;
            string imagename = Path.GetFileName(nameofimage);
            string filename = NameOfAnswerkey;
            filename = $"{root}\\{filename}";
            string name = $"{imagename.Substring(0, imagename.IndexOf('-'))} {imagename.Substring(imagename.IndexOf('-') + 1, imagename.IndexOf('.') - imagename.IndexOf('-') -1)}";
            string analysed = $"{name};{Punkte};{Math.Round(overall, 2)}%;{Grade}";
            
            if (!File.Exists($"{filename}.csv"))
            {
                exists = false;
            }
            using (FileStream fs = new FileStream($"{filename}.csv", FileMode.Append, FileAccess.Write))
            {
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    if (!exists)
                        sw.WriteLine("Name;Punkte von 4;Prozent;Note");
                    sw.WriteLine(analysed);
                    sw.Dispose();
                }
            }
        }
        public void ExportMultiple(string nameofimage, string Testname) // Mehrere
        {

            bool exists = true;
            string root = ExportPath;
            if (!Directory.Exists(root))
            {
                Directory.CreateDirectory(root);
            }

            int Punkte = Points[index];
            int Grade = Noten[index];
            double overall = Prozent[index];
            index++;
            string imagename = Path.GetFileName(nameofimage);
            string filename = Testname;
            filename = $"{root}\\{filename}";
            string name = $"{imagename.Substring(0, imagename.IndexOf('-'))} {imagename.Substring(imagename.IndexOf('-') + 1)}";
            string analysed = $"{name};{Punkte};{Math.Round(overall, 2)}%;{Grade}";

            if (!File.Exists($"{filename}.csv"))
            {
                exists = false;
            }
            using (FileStream fs = new FileStream($"{filename}.csv", FileMode.Append, FileAccess.Write))
            {
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    if (!exists)
                        sw.WriteLine("Name;Punkte von 4;Prozent;Note");
                    sw.WriteLine(analysed);
                    sw.Dispose();
                }
            }
        }

        public void ExportToCsv() // Alles aus Syntax nehmen
        {
            bool exists = true;
            string root = ExportPath;
            if (!Directory.Exists(root))
            {
                Directory.CreateDirectory(root);
            }
            string imagename = Path.GetFileName(inputImagePath);
            string filename = NameOfAnswerkey;
            filename = $"{root}\\{filename}";
            string name = $"{imagename.Substring(0, imagename.IndexOf('-'))} {imagename.Substring(imagename.IndexOf('-') + 1)}";
            string analysed = $"{name};{group1 + group2 + group3 + group4};{Math.Round(overall, 2)}%;{Note}";
            if (!File.Exists($"{filename}.csv"))
            {
                exists = false;
            }
            using (FileStream fs = new FileStream($"{filename}.csv", FileMode.Append, FileAccess.Write))
            {
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    if (!exists)
                        sw.WriteLine("Name;Punkte von 4;Prozent;Note");
                    sw.WriteLine(analysed);
                    sw.Dispose();
                }
            }
        }
        #endregion

        private Image<Bgr, byte> Calculation()
        {

            for (int i = 0; i < 3; i++)
            {
                if (this.Correctanswers[i] == answered[i])
                    group1++;
            }

            for (int i = 3; i < 6; i++)
            {
                if (this.Correctanswers[i] == answered[i])
                    group2++;
            }

            for (int i = 6; i < 9; i++)
            {
                if (this.Correctanswers[i] == answered[i])
                    group3++;
            }
            for (int i = 9; i < 12; i++)
            {
                if (this.Correctanswers[i] == answered[i])
                    group4++;
            }
            if (group1 == 3)
                group1 = 1;
            else
                group1 = 0;
            if (group2 == 3)
                group2 = 1;
            else
                group2 = 0;
            if (group3 == 3)
                group3 = 1;
            else
                group3 = 0;
            if (group4 == 3)
                group4 = 1;
            else
                group4 = 0;
            overall = (group1 + group2 + group3 + group4) / 4.0 * 100;

            if (overall >= 87.5)
                Note = 1;
            else if (overall >= 75)
                Note = 2;
            else if (overall >= 62.5)
                Note = 3;
            else if (overall >= 50)
                Note = 4;
            else
                Note = 5;

            
            imgout.Draw($"Punkte: {group1 + group2 + group3 + group4} von 4, Note: {Note}", new System.Drawing.Point(imgout.Width - 500, imgout.Height - 100), 0, 1, new Bgr(0, 255, 0), 2);
            return imgout;
        }
        private string CalculationMultiple()
        {

            for (int i = 0; i < 3; i++)
            {
                if (this.Correctanswers[i] == answered[i])
                    group1++;
            }

            for (int i = 3; i < 6; i++)
            {
                if (this.Correctanswers[i] == answered[i])
                    group2++;
            }

            for (int i = 6; i < 9; i++)
            {
                if (this.Correctanswers[i] == answered[i])
                    group3++;
            }
            for (int i = 9; i < 12; i++)
            {
                if (this.Correctanswers[i] == answered[i])
                    group4++;
            }
            if (group1 == 3)
                group1 = 1;
            else
                group1 = 0;
            if (group2 == 3)
                group2 = 1;
            else
                group2 = 0;
            if (group3 == 3)
                group3 = 1;
            else
                group3 = 0;
            if (group4 == 3)
                group4 = 1;
            else
                group4 = 0;
            overall = (group1 + group2 + group3 + group4) / 4.0 * 100;

            if (overall >= 87.5)
                Note = 1;
            else if (overall >= 75)
                Note = 2;
            else if (overall >= 62.5)
                Note = 3;
            else if (overall >= 50)
                Note = 4;
            else
                Note = 5;


            Points.Add(group1+group2+group3+group4);
            Noten.Add(Note);
            Prozent.Add(overall);
            // imgout.Draw($"Punkte: {group1 + group2 + group3 + group4} von 4, Note: {Note}", new System.Drawing.Point(imgout.Width - 500, imgout.Height - 100), 0, 1, new Bgr(0, 255, 0), 2);
            string imagename = Path.GetFileName(inputImagePath);
            string name = $"{imagename.Substring(0, imagename.IndexOf('-'))} {imagename.Substring(imagename.IndexOf('-') + 1, imagename.IndexOf('.') - imagename.IndexOf('-')-1)}";
            string analysed = $"Name: {name} | {group1 + group2 + group3 + group4} Punkte von 4 | Note: {Note}";
            return analysed;
        }



        #endregion
    }
}
