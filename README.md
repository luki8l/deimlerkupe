**MCT-Analyse**

Beschreibung:

Mithilfe dieses Projektes können einfach Multiple Choice Tests erstellt und ausgewertet werden. <br>
Das Erstellung erfolt über ein Canvas mit mehreren Eingabemöglichkeiten, die Auswertung wird
mittels digitaler Bildverarbeitung realisiert.

<br>
Verwendung:

<br>
<br>
Kontakt:
<br>
<br>
Lukas Lobnig, lukas.lobnig@student.htl-rankweil.at <br>
Noah Lorenzin, noah.lorenzin@student.htl-rankweil.at