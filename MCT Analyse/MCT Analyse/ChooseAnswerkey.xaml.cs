﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MCTCreateTempLib;
using MCTAnalysisLib;
using Path = System.IO.Path;
using WinForms = System.Windows.Forms; //FolderDialog

namespace MCT_Analyse
{
    
    /// <summary>
    /// Interaktionslogik für ChooseAnswerkey.xaml
    /// </summary>
    public partial class ChooseAnswerkey : Window
    {
        private bool[] selAnswerkey;

        public bool[] SelAnswerkey
        {
            get { return selAnswerkey; }
            set { selAnswerkey = value; }
        }
        private string testName;

        public string TestName
        {
            get { return testName; }
            set { testName = value; }
        }


        //public bool[] selectedanswerkey;
        int i = 0;
       
        public ChooseAnswerkey()
        {
            InitializeComponent();
            string path="";
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "CSV files (*.csv)|*.csv|XML files (*.xml)|*.xml";
            if (openFileDialog.ShowDialog() == true)
            {
                path = openFileDialog.FileName;
            }
            try
            {
                readanswerkey(path);
            }
            catch (Exception err)
            {

                MessageBox.Show(err.Message);
            }
            
            
        }

        

        public void readanswerkey(string path)
        {
            if (string.IsNullOrWhiteSpace(path))
                throw new Exception("Kein Pfad angegeben!");
            StreamReader sr = new StreamReader(path);
            string header = sr.ReadLine();
            while (!sr.EndOfStream)
            {
                string data = sr.ReadLine();
                ListAnswerkeys.Items.Add(AnswerkeyToString(data));
            }
            sr.Dispose();
        }

        public static string AnswerkeyToString(string data) //Diese Methode ist auch ohne Instanz nutzbar
        {
            string[] tokens = data.Split(';');
            string name = tokens[0];
            
            string answers = $"{tokens[1]},{tokens[2]},{tokens[3]},{tokens[4]},{tokens[5]},{tokens[6]},{tokens[7]},{tokens[8]},{tokens[9]},{tokens[10]},{tokens[11]},{tokens[12]}";

            return $"{name}: {answers}";
        }
        
        private void ListAnswerkeys_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            SelAnswerkey = new bool[12];
            string selected = ListAnswerkeys.SelectedItem.ToString();
            string name = selected.Substring(0, selected.IndexOf(':'));
            string answers = selected.Substring(selected.IndexOf(':')+1);
            string[] tokens = answers.Split(',');
            foreach (string item in tokens)
            {
                SelAnswerkey[i] = bool.Parse(item);
                i++;
            }
            TestName = name;
            this.Close();
            
        }
    }
}
